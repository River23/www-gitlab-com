module TeamHelpers
  def direct_team(manager_role:)
    members = Gitlab::Homepage::Team.new.direct_team(manager_role: manager_role)

    partial('includes/team_member_table', locals: { team_members: members })
  end

  # Stable counterparts for a team, found by matching roles against
  # role_regexp. Anyone in the direct team reporting to direct_manager_role will
  # be excluded.
  def stable_counterparts(role_regexp:, direct_manager_role:)
    members =
      Gitlab::Homepage::Team.new.role_matches(role_regexp: role_regexp) -
      Gitlab::Homepage::Team.new.direct_team(manager_role: direct_manager_role)

    partial('includes/team_member_table', locals: { team_members: members })
  end
end
