---
layout: markdown_page
title: "Agile Delivery with GitLab"
---
## Agile Development and Project Management
Since the publication of the [2001 Agile Manifesto](http://agilemanifesto.org/), development teams have developed itterative, incremental and lean approaches to streamline and accelerate the delivery of modern software projects.  The techniques have ranged from 'extreme programming' to [Scrum](https://www.scrum.org/) and [Kanban](https://en.wikipedia.org/wiki/Kanban_(development)) where teams are able to rapidly organize, plan and deliver working software.   Large enterprises have adopted agile at enterprise scale in many frameworks, ranging from "[Scaled Agile Framework (SAFe)](https://www.scaledagile.com/)" to [Disciplined Agile Delivery](http://www.disciplinedagiledelivery.com/)

GitLab enables teams to apply agile practices and principles to organize and manage their work.

## Scrum

Scrum is an agile development framework where teams establish a consistent cadence to organize and deliver value. A scrum team is cross functional development team, which takes their cues from the Product Owner and the Scrum Master is responsible to help protect the team from external distractions. [What is Scrum](https://www.scrum.org/resources/what-is-scrum)  The Scrum team breaks their work into short increments of Sprints ranging from a week to several weeks, where the team focuses on delivering value (working software) that meets the needs of the Product Owner.  

**Scrum Events**

| Scrum Event | Scrum in GitLab |
|------------------------|-----------------|
| Sprint | A sprint represents a finite time period in which the work is to be completed, which may be a week, a few weeks, or perhaps a month or more. The product owner and the development team meet to decide work that is in scope for the upcoming sprint. GitLab's milestones feature supports this: assign milestones a start date and a due date to capture the time period of the sprint. The team then puts issues into that sprint by assigning them to that particular milestone.  |
| Sprint Planning | User stories describe the scope, goals and objectives of new functionality.  User stories also drive an estimation of the technical effort to implement the story. In GitLab, issues are where user stories are captured and issues have a weight attribute, which is where to indicate the estimated effort.  User stories are further broken down to technical deliverables, sometimes documenting technical plans and architecture. In GitLab, this information can be documented in the issue, or in the merge request description, as the merge request is often the place where technical collaboration happens.   During the sprint (GitLab milestone), development team members pick up user stories to work on, one by one. In GitLab, issues have assignees. So you would assign yourself to an issue to reflect that you are now working on it. We'd recommend that you create an empty and linked-to-issue merge request right away to start the technical collaboration process, even before creating a single line of code. |
| Daily Scrum |    Throughout the sprint, issues move through various stages, such as Ready for dev, In dev, In QA, In review, Done, depending on the workflow in your particular organization. Typically these are columns in an Agile board. In GitLab, issue boards allow you to define your stages and enable you to move issues through them. The team can configure the board with respect to the milestone and other relevant attributes. During daily standups, the team looks at the board together, to see the status of the sprint from a workflow perspective. |
| Sprint Review | The development team stays on track in real time, and mitigates risks as they arise. GitLab provides burndown charts, allowing the team to visualize the work scoped in the current sprint "burning down" as they are being completed.   Toward the end of the sprint, the development team demos completed features to various stakeholders. With GitLab, this process is made simple using Review Apps so that even code not yet released to production, but in various testing, staging or UAT environments can be demoed. Review Apps and CI/CD features are integrated with the merge request itself. These same tools are useful for Developers and QA roles to maintain software quality, whether through automated testing with CI/CD, or manual testing in a Review App environment. |
| Sprint Retrospective | tbd |

**Scrum artifacts**

| Scrum Artifact | Scrum in GitLab |
|------------------------|-----------------|
| Product Backlog | The product or business owners typically create these user stories to reflect the needs of the business and customers. They are prioritized in a product backlog to capture urgency and desired order of development. The product owner communicates with stakeholders to determine the priorities and constantly refines the backlog. In GitLab, there are dynamically generated issue lists which users can view to track their backlog. Labels can be created and assigned to individual issues, which then allows you to filter the issue lists by a single label or multiple labels. This allows for further flexibility. Priority labels can even be used to also order the issues in those lists. |
| Sprint Backlog | In Agile, you often start with a user story that captures a single feature that delivers business value for users. In GitLab, a single issue within a project serves this purpose. |
| Increment| tbd |



## Kanban
[Kanban](https://en.wikipedia.org/wiki/Kanban_(development)) is a software delivery approach based on the lean manufacturing principles of visualizing work in order to manaage the 'work in proceess' (WIP) and reduce non value activities and waste.  The goal of Kanban is to focus on flow of value and allow a team to quickly re-prioritize work based on customer demand.


**Kanban artifacts**

| Kanban Artifact | Kanban in GitLab |
|------------------------|-----------------|
| Kanban Board | In GitLab, issue boards can easily be configured to support Kanban delivery practices.  The key is to pay close attention to the capacity of the team in each stage of the work and to leverage the [issue weights](https://docs.gitlab.com/ee/user/project/issue_board.html#sum-of-issue-weights) of all the use cases (issues) in process at any point of time. |
| Backlog | tbd |
| * | tbd |

## SAFe


## Jira integration

## Resources
 * [GitLab for Agile](https://about.gitlab.com/2018/03/05/gitlab-for-agile-software-development/)

 * [4 ways to use issue boards](https://about.gitlab.com/2018/08/02/4-ways-to-use-gitlab-issue-boards/)

 *  [!GitLab project mgt youtube](https://www.youtube.com/watch%3Fv%3DLB5zvfjIDi0&ved=2ahUKEwjw2cGf5-rdAhUkWN8KHeE8BJAQjjgwA3oECAUQAQ&usg=AOvVaw1yAMfhvN3pjNciSoNUmDjQ)

 * [!GitLab, Jira and Jenkins youtube](https://www.youtube.com/watch%3Fv%3DJn-_fyra7xQ&ved=2ahUKEwjw2cGf5-rdAhUkWN8KHeE8BJAQjjgwE3oECAEQAQ&usg=AOvVaw3jCWmPspuIWQqz0YfMwRG1)

[!GitLab and atlassian](https://www.youtube.com/watch%3Fv%3Do7pnh9tY5LY&ved=2ahUKEwjgrJ-I6OrdAhWpc98KHSRMCSwQjjgwBHoECAgQAQ&usg=AOvVaw0fHEQPLR8-oJnaweIwXQdo)